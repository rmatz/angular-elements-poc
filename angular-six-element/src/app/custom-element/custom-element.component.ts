import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-custom-element',
  templateUrl: './custom-element.component.html',
  styles: [],
  encapsulation: ViewEncapsulation.Native
})
export class CustomElementComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
