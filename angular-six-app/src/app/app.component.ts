import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  ngOnInit() {
    this.loadWebComponentsModule();
  }

  async loadWebComponentsModule() {
    const rootUrl = 'http://localhost:4010';
    await this.loadScript(`${rootUrl}/runtime.js`);
    await this.loadScript(`${rootUrl}/vendor.js`);
    await this.loadScript(`${rootUrl}/main.js`);
  }

  private async loadScript(url) {
    return new Promise(resolve => {
      const script = document.createElement('script');
      script.setAttribute('src', url);
      script.onload = () => resolve();
      document.body.appendChild(script);
    });
  }
}
